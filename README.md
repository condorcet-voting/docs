# Condorcet: 
# A Peer-to-Peer Electronic Voting System
*François Jolain*


## Abstract

**Democracy is based on voting, but current voting system is a mess. Reliable electronics voting system for this new century becomes a huge issue.**

**Hopefully, a ring signature can prove the authentication from a legit group of voters without reveal the original signatory identity. El-Gamal cypher can construct a public key from several private keys, a voter can force a group of nodes to reach the unanimity before decrypt his ballot.
 And blockchain ecosystem release dedicated hardware device like Ledger to avoid trust website or mobile phone or even Operating System.**

**These technologies can be combined to create electronics voting system. However, the main issue is performed ring sign with millions of voters. A smart way is described in this paper to sample public keys used for ring signature.**

## Introduction
Current voting system is a mess. Voters must trust organizer, despite the organizer (like a government) is often a candidate to this own balloting.
This situation makes the vote result completely obsolete if government is corrupted.

Future of voting system seems worst than ballot system or even postal vote. Voter goes to a computer entirely controlled by Organizer to make a choice on it. This system enables just the organizer to cheat and still citizen vote with even less effort than previous system. This system is such dump, it can be hacked by other state too…

Hopefully, new signature technologies like ring signature, El-Gamal, and new security device like hardware device can be used to build a reliable electronic voting system.

### Ring signature
A ring signature can merge an individual signature with many peoples to create a group signature. A ring signature starts by only one signatory, who use its private key to sign, like a classic RSA signature. But he can add other people public key inside its signature to dilute it. At the end, no one can find the primary signatory. And secondaries signatories don’t need to participate to this signature.

### El-Gamal cypher
Like any asymmetric cipher, El-Gamal provides public key to let encrypt a message. But El-Gamal can create a public key from multi-private keys, where all private keys are needed to decrypt the message.

### Hardware device
Secure private key was a big issue at the beginning of Bitcoin. Solution must be strong and most user-friendly as possible. Ledger company released the ledger nano S in this topic. This device is basically a secure chip card with its own reader, device embeds screen and button next to the secure chip. This little step forward enable user to see on its personal device what will be signed. No need to make blind signature like for credit card system.
Thanks to this security hardware wallet, you just need to trust the manufacturer to display the same information seem on screen and send to chip. You don’t need to trust any laptop, smartphone, software, or website connected to your hardware wallet.

### Nomenclature
- ORGANISER: people(s) or institution who organize the vote
- VOTER: people known as eligible by the ORGANISER to vote.
- MANUFACTURER: company who make and distribute a hardware device.
- NODE: people who run a node of voting system.

### Requirements
Let’s build a voting system with these technologies. Voting system must respect these requirements:
1. ORGANIZER could ask VOTER identity to verify its eligibility
2. ORGANIZER can’t know the VOTER behind a vote
3. ORGANIZER can’t delete or change vote made by a VOTER
4. ORGANIZER can’t stuff the ballot box
5. VOTER can only vote once
6. VOTER ballot is encrypted until the end of voting
7. VOTER don’t need to trust ORGANIZER


# Step 1: Registration
ORGANIZER gives to hardware device the vote ID, hardware device derivate master vote key with vote ID. VOTER gives its identity and pub key to ORGANIZER. If ORGANIZER accept VOTER as eligible, then a new row is insert into the registration database.

[![](https://mermaid.ink/img/pako:eNpt0cFuAiEQANBfIXN1_QEOJk13YzzUbbTxYLhMYapkC2xZMDHGf5eyXWpqOcHMm0yYuYB0ioDDQF-RrKRa48GjEZal06MPWuoebWDrtm62j-F2s3xar_bN5jG1a9-mcFHzxWJWHpw9H0l2I2Sr-h-bU5wt9YnYzgUqaqy5IzV5fcIkDA6BPOvo_FfeNX6N77-ixLPKH-WsHwVUYMgb1CoN6fLtBYQjGRLA01Wh7wQIe00u9ir1b5QOzgP_wM-BKsAY3PZsJfDgI03oZ8pFUS56GVeRN1JBmuHeuclcbwGghfM?type=png)](https://mermaid.live/edit#pako:eNpt0cFuAiEQANBfIXN1_QEOJk13YzzUbbTxYLhMYapkC2xZMDHGf5eyXWpqOcHMm0yYuYB0ioDDQF-RrKRa48GjEZal06MPWuoebWDrtm62j-F2s3xar_bN5jG1a9-mcFHzxWJWHpw9H0l2I2Sr-h-bU5wt9YnYzgUqaqy5IzV5fcIkDA6BPOvo_FfeNX6N77-ixLPKH-WsHwVUYMgb1CoN6fLtBYQjGRLA01Wh7wQIe00u9ir1b5QOzgP_wM-BKsAY3PZsJfDgI03oZ8pFUS56GVeRN1JBmuHeuclcbwGghfM)

`REGISTRATION table`

| Identity | Pub Key  | Vote ID |
|----------|----------|---------|
| John Doe | 0x123456 | US-2022 |

### Issue #1: Stuff the ballot box
ORGANIZER can fill missing registrations with controlled keys. This issue can’t be fully avoided, but drastically reduce by using authenticated hardware device. MANUFACTURER will generate inside the device a manufacturer auth key link to a master key by an embedded certificate.

When registration, hardware device sign pub vote key with auth key and give the full chain certificate to prove the VOTER has received a hardware device.

[![](https://mermaid.ink/img/pako:eNp90sFugzAMANBfiXwd-4EcKk0DVT2sVGXqoeLiBVMiIGEh6VRV_fdlYTA0quaUOM-yI-cKQhcEHHr6dKQExRJPBttcMb86NFYK2aGybJvGSbYMp_v1y3ZzTPbLq0P6PoYn9bxaPU0Hzl4rEvUA2Sa-Y8MVZ2t5JnbQliY15MxITEae0YsWe0uG1XT5L2eFd-7jT0zxoMJDOevmYlEtkyc1EvYlbcUE-ZeXUvgWHtQtXdMwUaFUIeFB_bsSImjJtCgLP7LrTywHW1FLOXC_LdDUOeTq5p3rCt9KUkirDfASm54iQGd1dlECuDWORvQ780lRSHobPkb4HxH4iR61Hs3tGzahsLg?type=png)](https://mermaid.live/edit#pako:eNp90sFugzAMANBfiXwd-4EcKk0DVT2sVGXqoeLiBVMiIGEh6VRV_fdlYTA0quaUOM-yI-cKQhcEHHr6dKQExRJPBttcMb86NFYK2aGybJvGSbYMp_v1y3ZzTPbLq0P6PoYn9bxaPU0Hzl4rEvUA2Sa-Y8MVZ2t5JnbQliY15MxITEae0YsWe0uG1XT5L2eFd-7jT0zxoMJDOevmYlEtkyc1EvYlbcUE-ZeXUvgWHtQtXdMwUaFUIeFB_bsSImjJtCgLP7LrTywHW1FLOXC_LdDUOeTq5p3rCt9KUkirDfASm54iQGd1dlECuDWORvQ780lRSHobPkb4HxH4iR61Hs3tGzahsLg)

`REGISTRATION table`

| Identity | Pub Key | Vote ID | full chain certificate |
|----------|---------|---------|------------------------|
| John Doe | 0xKEY   | US-2022 | 0xCERTIFCATE           |



## Step 2: Vote
VOTER connects its hardware device to a website or voting machine. ORGANIZER send to device list of registered pub keys and user voting choice. Device will ask to VOTER to confirm its vote on the embedded screen. Then device will perform and return a ring signature with VOTER choice as payload.

[![](https://mermaid.ink/img/pako:eNplkD2OwjAQha8ympZwARdUSbHFggSIYuVmsGeDRWIHe1wgxN3XOLAFuBo_fW9-3g1NsIwKE18ye8Otoz7SqD2UN1EUZ9xEXmC9abvdp3zY7LvtLFdiuVotqqbgEIThq4UFGPLWWRJO5TPlI5z5mmZTZeHdVXhIrvfvTB2h4EjDEKT0is73FSTJkbHBkeNIzpaDbg-vRjnxyBpVKS3Fs0bt74XL02OdzjoJEdUvDYkbpCxhd_UGlcTML-iZyD_F1fQ9x1bTa7BE8RPCi7n_Af-NbxQ?type=png)](https://mermaid.live/edit#pako:eNplkD2OwjAQha8ympZwARdUSbHFggSIYuVmsGeDRWIHe1wgxN3XOLAFuBo_fW9-3g1NsIwKE18ye8Otoz7SqD2UN1EUZ9xEXmC9abvdp3zY7LvtLFdiuVotqqbgEIThq4UFGPLWWRJO5TPlI5z5mmZTZeHdVXhIrvfvTB2h4EjDEKT0is73FSTJkbHBkeNIzpaDbg-vRjnxyBpVKS3Fs0bt74XL02OdzjoJEdUvDYkbpCxhd_UGlcTML-iZyD_F1fQ9x1bTa7BE8RPCi7n_Af-NbxQ)


`BALLOT table`

| vote  | List of used pub keys | Vote Signature |
|-------|-----------------------|----------------|
| "yes" | 0x123, 0x456          | 0xSIGNATURE    |

### Issue #2: vote more than once
Each list of registered pub keys given to VOTER, will create a legit vote. Therefore, it’s important to reduce the official list of pub keys to exactly one for each VOTER, otherwise VOTER can vote many times. VOTER must also sign this list of pub keys, this signature should be unique in Database, otherwise VOTER can vote multi-choice.

[![](https://mermaid.ink/img/pako:eNptkTFuwzAMRa9CcI17AQ2Z7KFDG6AJMhRaGItxhdiSK1FDEOTuleU6MNJqkr7e_wTJG7beMCqM_J3YtVxb6gIN2kE-IwWxrR3JCbzv6mb_Vz7uDs3HLBfiZbvdFE3B0QvDaw0baMkZa0g45seYTnDha5xNhYVnV-Yh2s49M6WEghP1vZecFazrCkiSAq_oVeD0Db2NAv78X_FV7iNpQotl4bHCgcNA1uRZ3Sa3RvnigTWqfDUULhq1u2cujVOnjbHiA6oz9ZErpCR-f3UtKgmJF-h32A-Ki-lt3khZTIV5yp_eL8z9Byrzi9g?type=png)](https://mermaid.live/edit#pako:eNptkTFuwzAMRa9CcI17AQ2Z7KFDG6AJMhRaGItxhdiSK1FDEOTuleU6MNJqkr7e_wTJG7beMCqM_J3YtVxb6gIN2kE-IwWxrR3JCbzv6mb_Vz7uDs3HLBfiZbvdFE3B0QvDaw0baMkZa0g45seYTnDha5xNhYVnV-Yh2s49M6WEghP1vZecFazrCkiSAq_oVeD0Db2NAv78X_FV7iNpQotl4bHCgcNA1uRZ3Sa3RvnigTWqfDUULhq1u2cujVOnjbHiA6oz9ZErpCR-f3UtKgmJF-h32A-Ki-lt3khZTIV5yp_eL8z9Byrzi9g)

`BALLOT table`

| Vote  | List of used pub keys | Vote Signature   | list pub keys signature |
|-------|-----------------------|------------------|-------------------------|
| "yes" | 0x123, 0x456          | 0xSIGNATURE_VOTE | 0xSIGNATURE_PUBKEYS     |



## Issue #3: Huge amount registrations
Previous issue can be easy fixed if we use the whole list of registered pub keys. A legit vote must use the whole list, there is only one official list for each user, therefore VOTER can vote only once.

However, if there are thousands, or even millions of VOTERS, the size of whole list, like the compute time to perform a ring signature, is prohibited. We need to use sub list of pub keys.

## Issue #4 break vote anonymous by scoped sub list of keys
We must split the whole list in sub list if too big. If VOTER choose an arbitrary list, it can vote many times as describe in issue #3.

However, if ORGANIZER can choose sub list of pub keys send to VOTER, he can break the anonymous. He can create only mock keys controlled by him. If VOTER, sign this ring signature, ORGANIZER can retrieve the only true signatory inside.

To fixe these issues, we can split registered pub keys in a smarter way.
If we consider:

- $L$ the length of the whole list of registered pub keys
- $S_L$ the length of the sub list keys
- $\underline{S_L}$ the minimal length of a sub list to protect VOTER anonymous
- $B_n$ the first $n$ bits of VOTER key

Then we can, extract from the whole list only the pub keys which match the $B_n$ of VOTER key.
To ensure the sub list is bigger or equals than $\underline{S_L}$,

$$
\begin{array}{rcl}
S_L & > & \underline{S_L} \\
L/2^n & > & \underline{S_L} \\
L/\underline{S_L} & > & 2^n \\
\log_2{\left(L/\underline{S_L}\right)} & > & n \\
n & = & floor \left[ \log_2{\left(L/\underline{S_L}\right)} \right]
\end{array}
$$

The official sub list is not chosen by ORGANIZER to avoid issue #4, and there is only one official list for VOTER to avoid issue #3.

`BALLOT table`

| Vote  | Bn               | Vote Signature   | list pub keys signature |
|-------|------------------|------------------|-------------------------|
| "yes" | 0bFIRST_KEY_BITS | 0xSIGNATURE_VOTE | 0xSIGNATURE_PUBKEYS     |

## Step 3: Results
### Issue #5 Preserve vote result until end.
### Issue #6 Avoid a malicious vote to be published
Currently, each ballot is public as soon as broadcast on nodes. This could affect future votes and at end vote result. Another issue comes because VOTER has to valid the sub list pub key him-self. If a ballot is broadcast with the wrong sub list pub keys, ballot is take off from result, but if the sub list was forged by attacker, he can know the VOTER identity and his ballot as seen in issue #4.

These two issues is resolve by encrypt the VOTER ballot with an El-Gamal asymmetric cipher and sign the ciphered ballot.

Like any asymmetric cipher, El-Gamal provides public key to let VOTERS encrypt its ballot. But El-Gamal can create a multi-private key. Each node has its private key. Before start voting, nodes merge their public key together to create the main public key used to encrypt ballots.

To know VOTER result, ballot has to be decrypted by each node one by one. If only one node detect than the signature is not done with the good sub list, it refuses to decrypt and keep the VOTER ballot encrypted. VOTER ballot is still take off, but his ballot anonymity is preserve.

Moreover, VOTER can choose his trusted NODES. Only one honest NODE inside the selected VOTER NODES guarantee anonymity.

[![](https://mermaid.ink/img/pako:eNplz7EOgjAQBuBXaW4VB2HrwASjOLCZLmd7KpG2WNqBEN7dikKI3nTD91_uH0FaRcChp2cgI6lo8OZQC8PiVKeiZId9nu_mLeVMkXRD59kF29Z6tmHpyrJftlHZqqq_Y5CAJqexUfGf8R0S4O-kSQCPq0L3ECDMFF3oFHoqVeOtA37FtqcEMHhbD0YC9y7Qgr6FVkVz6PhpPZdPoENztnYx0wuHNVje?type=png)](https://mermaid.live/edit#pako:eNplz7EOgjAQBuBXaW4VB2HrwASjOLCZLmd7KpG2WNqBEN7dikKI3nTD91_uH0FaRcChp2cgI6lo8OZQC8PiVKeiZId9nu_mLeVMkXRD59kF29Z6tmHpyrJftlHZqqq_Y5CAJqexUfGf8R0S4O-kSQCPq0L3ECDMFF3oFHoqVeOtA37FtqcEMHhbD0YC9y7Qgr6FVkVz6PhpPZdPoENztnYx0wuHNVje)

`VOTE table`

| Vote   | Bn           | Vote Signature | list pub keys signature | Node keys      |
|--------|--------------|----------------|-------------------------|----------------|
| 0xVOTE | 0bN_KEY_BITS | 0xSIGN_VOTE    | 0xSIGN_PUBKEYS          | 0xKEY1, 0xKEY2 |


`NODE table`

| website | pub key  | certificate   |
|---------|----------|---------------|
| eff.org | 0xPUBKEY | 0xCERTIFICATE |

*PS: voting system with El-Gamal is already used in [EPFL intern votes](https://www.epfl.ch/labs/dedis/wp-content/uploads/2020/01/report-2017-2-andrea_caforio-evoting.pdf). But they use a shuffle to protect anonymity that can't scale with thousands of VOTERS.*
